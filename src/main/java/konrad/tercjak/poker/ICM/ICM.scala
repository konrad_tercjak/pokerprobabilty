package konrad.tercjak.poker.ICM

import scala.ref.WeakReference


class ICM( var rewards:Array[Double]){
  import scala.collection.mutable

  private val cache =mutable.Map.empty[Array[Double], ref.WeakReference[EV]]

  def getEV(chips: Array[Double]): Array[Double] = synchronized {
    cache.get(chips) match {
      case Some(WeakReference(newObj)) => newObj.ev
      case _ =>
        val newObj = new EV(chips)
        cache.put(chips, WeakReference(newObj))
        newObj.ev
    }
  }

 private class EV(chips: Array[Double]) {
    val ev: Array[Double] = new Array(chips.size)

   val playersNum = chips.size - 1

   def run(N: Int, pos: Seq[Int], topChance: Double, sum: Double, rewardID: Int): Unit = {
     for (j <- 0 to playersNum if (pos.forall(x => j != x))) {
       var reward = rewards(rewardID)
       var newchance = chips(j) / sum * topChance
       var newpos = j +: pos
       var newSum = sum - chips(j)
       ev(j) = ev(j) + newchance * reward
       if (N != 0) {
         run(N - 1, newpos, newchance, newSum, rewardID + 1)
       }

     }
   }

   run(rewards.size, Seq[Int](), 1.0, chips.sum, 0)

//   ev.foreach(println(_))

   def print: Unit ={
      println(ev.foldLeft("")(_+_.toString+","))
    }

  }

//  def setRewards(_rewards:Array[Double]):Unit={
//    rewards=_rewards
//  }

}


