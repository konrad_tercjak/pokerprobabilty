//package konrad.tercjak.poker.ICM
//
//class ICM_Decisions (chips:Array[Double],rewards:Array[Double]) {
//  var icm=new ICM_EV(chips,rewards)
//  var icmb=new ICM_EV(chips,rewards)
//  var icmc=new ICM_EV(chips,rewards)
//
//  var evC2=0.0
//  def getDecision(heroSeat: Int,oponentPlace: Int,shared:Double,bet:Double,probW:Double,chipsAfter:Array[Double]) :Boolean= {
//
//    var chipsW=chipsAfter.clone // .clone
//    var chipsL=chipsAfter.clone
//    var chipsF=chipsAfter.clone
//    var probL=1-probW
//    //you call & win
//    chipsW(heroSeat)+=shared
//    //call & lose
//    chipsL(heroSeat)-=bet
//    chipsL(oponentPlace)+=shared+bet
//    //you fold
//    chipsL(oponentPlace)+=shared
//    var evW=icm.startWith(chipsW)(heroPlace)
//    var evL=icmb.startWith(chipsL)(heroPlace)
//    var evF=icmc.startWith(chipsF)(heroPlace)
//
//    evC2=evW*probW +evL*probL
//
//    evC2>evF
//  }
//
//  def getCalling_Range(heroPlace: Int,oponentPlace:Int,shared:Double,bet:Double):Double ={
//    var left=0.5
//    var right=1.0
//    var mid=(right+left)/2
//    var	probW=mid
//    var chipsBefore=chips.clone
//
//    chipsBefore(oponentPlace)=chipsBefore(oponentPlace)-bet
//    for( i <- 0 to 10){
//      if(getDecision(heroPlace,oponentPlace,shared,mid,probW,chipsBefore)){
//        right=mid														//Call too much
//        mid=(right+left)/2
//        probW=mid
//      }
//      else{														//Fold too small
//        left=mid
//        mid=(right+left)/2
//        probW=mid
//      }
//    }
//    while(!getDecision(heroPlace,oponentPlace,shared,mid,probW,chipsBefore)&&probW<1){
//      probW+=0.01
//    }
//    println("evC="+evC2)
//    println("probW="+probW)
//
//    probW
//  }
//
//  def getCall_Ranges() :Unit= { //0.5 1=> 0.75 too small
//    var probC12=getCalling_Range(0,1,10.0,10.0)
//    println("P1:"+probC12)
//    var probC31=getCalling_Range(2,0,10.0,10.0)
//    println("P3:"+probC31)
//    var probC10=getCalling_Range(1,0,10.0,10.0)
//    println("P2:"+probC10)
//  }
//}