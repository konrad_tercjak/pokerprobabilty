package konrad.tercjak.poker.ICM
import konrad.tercjak.util.Bisection.bisectionP
class Decisions(rewards:Array[Double],chipsAfter:Array[Double]) {
  val icm=new ICM(rewards)
//  icm.getEV(chips)

  def getCallEV( heroID: Int,
                   villianID: Int,
                   bet:Double,
                   basePot:Double=0.0,
                   )
            (probW:Double) :Double= {

    val potC=basePot+bet
//    val potF=basePot+bet


    var chipsW=chipsAfter.clone
    var chipsL=chipsAfter.clone
//    val chipsF=chipsAfter.clone

    val probL=1-probW

    //you call & win
    chipsW(heroID)+=potC
    chipsW(villianID)-=bet


    //you call & lose
    chipsL(heroID)-=bet
    chipsL(villianID)+=potC



    val evW=icm.getEV(chipsW)(heroID)
    val evL=icm.getEV(chipsW)(heroID)
//    val evF=icm.getEV(chipsW)(heroID)

   val evC=evW*probW +evL*probL


    evC
  }
  def getCallRange(heroID: Int,
                   villianID: Int,
                   bet:Double,
                   basePot:Double=0.0):Double= {
    val chipsF=chipsAfter.clone
    //you fold & villian win
    val potF=basePot+bet
    chipsF(villianID)+=potF

    val evF=icm.getEV(chipsF)(heroID)

    bisectionP(getCallEV(heroID: Int,
      villianID: Int,
      bet: Double,
      basePot))(0.01,evF)


  }
}
