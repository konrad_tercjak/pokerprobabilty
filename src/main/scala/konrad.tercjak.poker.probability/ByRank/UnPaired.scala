package konrad.tercjak.poker.probability.ByRank

import konrad.tercjak.poker.probability.BasicProbability.Kinds
import konrad.tercjak.poker.probability.FlopProbabilty._

object UnPaired {    //Common odds when holding unpaired hole cards:
  val paired=Kinds(2,3)  //matchKind
  val unPaired=Kinds(13-2,4)//otherKind


  val pairing1HoleCard=paired.ChooseSame(1)

  var pairing1HoleCardAndNotPairingBoard =
    pairing1HoleCard* unPaired.ChooseDiff(2) /flopCards

  var pairing1HoleCardAndPairingBoard =
    pairing1HoleCard * unPaired.ChooseSame(2) /flopCards

  var pairing2HoleCards=
    paired.ChooseDiff(2) * unPaired.ChooseDiff(1) /flopCards

  var trips1HoleCard=
    paired.ChooseSame(2)*  unPaired.ChooseDiff(1)/flopCards

  var fullhouseTripsAndPairHoleCards=
    paired.ChooseSame(2)*Kinds(1,3).ChooseSame(1)/flopCards

  var fourkind=
    paired.ChooseSame(3)/flopCards

  var probs= List(	pairing1HoleCardAndNotPairingBoard,
    pairing1HoleCardAndPairingBoard,
    pairing2HoleCards,
    trips1HoleCard,
    fullhouseTripsAndPairHoleCards,
    fourkind	)



  var table:List[List[String]]=names.zip(probs.map(x=>getPercentFormated(x))).map(x=>List(x._1,x._2))

  def print: Unit ={
    println(s"Common odds when holding Unpaired hole cards:" )
    println(info )
    printTable(table)
  }



}