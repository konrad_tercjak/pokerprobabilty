package konrad.tercjak.poker.probability.ByRank

import konrad.tercjak.poker.probability.BasicProbability._
import konrad.tercjak.poker.probability.FlopProbabilty._

object Paired {       //Common odds when holding paired hole cards:
  val paired=Kinds(1,2)  //matchKind
  val unPaired=Kinds(13-1,4)

  var twoPairsByPairingBoards =
    unPaired.ChooseSame(2) *Kinds(13-2,4).ChooseDiff(1) /flopCards

  var tripsHoleCards =
    paired.ChooseSame(1) *unPaired.ChooseDiff(2) /flopCards

  //set to your hole pair and pairing the board
  var fullhouseByTripsHoleCardsAndPairingBoard =
    paired.ChooseSame(1)*unPaired.ChooseSame(2)/flopCards

  var fullhouseByTripsBoard =
    unPaired.ChooseSame(3)/flopCards

  var fourkind =
    paired.ChooseSame(2)*unPaired.ChooseDiff(1)/flopCards


  var probs= List(	twoPairsByPairingBoards,
    tripsHoleCards,
    fullhouseByTripsHoleCardsAndPairingBoard,
    fullhouseByTripsBoard,
    fourkind	)



  var table=names.zip(probs.map(x=>getPercentFormated(x))).map(x=>List(x._1,x._2))

  def print: Unit ={
    println(s"Common odds when holding PAIRED hole cards:" )
    println(info )
    printTable(table)
  }
}