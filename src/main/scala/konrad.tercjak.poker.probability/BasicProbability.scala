package konrad.tercjak.poker.probability

import java.math.BigInteger

object BasicProbability {
  implicit class Permutation(n: Int) {

    @annotation.tailrec
    private def fact(number: BigInt, result: BigInt = 1): BigInt = {
      if (number == 0){result}
      else{fact(number -1, result * number)}
    }

    def ! = fact(n) // allows 10!
//    def choose(k: Int): BigInt = fact(n) / (fact(n - k) * fact(k))

  }

   def binomial(  N:Int,  K:Int):BigInteger= {
    var ret = BigInteger.ONE
    for (k <- 0 until K) {
      ret = ret.multiply(BigInteger.valueOf(N-k))
        .divide(BigInteger.valueOf(k+1));
    }
    ret
  }
  def C(n:Int,k:Int):Double=choose(n,scala.math.min(k,n-k))
  def choose(n:Int,k:Int):Double={
    var c: Double=1
    var j:Double=n
    var kk= scala.math.min(k,n-k)
    for(i <- 1 to kk){
      c= c*j/i //n/1 *2(n-1)/2
      j= j-1
    }
    c

  }
  case class Kinds(kind:Int,clones:Int){
    def ChooseDiff(n:Int):Double = {
      C(kind,n)*Math.pow(C(clones,1),n)
    }
    def ChooseSame(n:Int): Double= {
      C(kind,1)*C(clones,n)
    }
  }
}
