package konrad.tercjak.poker.probability.BySuit
import konrad.tercjak.poker.probability.BasicProbability._
import konrad.tercjak.poker.probability.FlopProbabilty._

object Suited  {
  var suited=Kinds(1,11)
  var unSuited=Kinds(3,13)

  var fourFlush =
    suited.ChooseSame(2)*unSuited.ChooseDiff(1)/flopCards

  var flush =
    suited.ChooseSame(3)/flopCards

  var names=List("Flush","Flopping a four flush ")

  var probs=List(flush,fourFlush)

  var table=names.zip(probs.map(x=>getPercentFormated(x))).map(x=>List(x._1,x._2))

  def print: Unit ={
    println(s"Common odds when holding SUITED hole cards:" )
    println(info )
    printTable(table)
  }
}