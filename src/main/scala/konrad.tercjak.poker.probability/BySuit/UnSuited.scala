package konrad.tercjak.poker.probability.BySuit
import konrad.tercjak.poker.probability.BasicProbability._
import konrad.tercjak.poker.probability.FlopProbabilty._

object UnSuited  {
  var suited=Kinds(2,12) // 2 diff ranks with 3 diff suits
  var otherKind=Kinds(2,13) //other kind

  val fourFlush  =
    suited.ChooseSame(3)/flopCards

  val probs=List(fourFlush)
  var names=List("Flopping a four flush ")

  var table=names.zip(probs.map(x=>getPercentFormated(x))).map(x=>List(x._1,x._2))

  def print: Unit ={
    println(s"Common odds when holding UnSuited hole cards:" )
    println(info )
    printTable(table)
  }
}