package konrad.tercjak.poker.probability
import konrad.tercjak.poker.probability.BasicProbability._
import konrad.tercjak.util.Tabulator
object FlopProbabilty {

  val flopCards=C(50,3)
  val headerList=List("Name", "Probabilty")

  var names=List(	"~ one pair by pairing a hole card " ,
      "~ two pair by pairing a hole card AND pairing on the board" ,
      "~ two pair by pairing EACH of your hole cards ",
      "~ trips by flopping two cards to one hole card",
      "~ a full house, trips of one hole card and pairing the other",
      "~ four of a kind, three cards to one of your hole cards "
    )

  var info="\\'~' means flopping EXACTLY"

  def getPercentFormated(probabilty:Double):String={
    "%.4f".format(probabilty*100)+"%"
  }

  def printTable(table:List[List[String]]):Unit={
   println(Tabulator.format(List(headerList):::table))

  }
  def  pokerCombinations(ranks: Int,suit:Int,cards:Int,chosenRanks:Int,chosenSuits:Int) = {
    C(ranks , chosenRanks)*math.pow(C(suit , chosenSuits),cards)
    //	(3 C 1)*    (3 C 1)*   (11 C 2)* math.pow((4 C 1),2) //not triple or 4kinf


  }

//  scala>  (13 C 1)*(4 C 2)*(12 C 3)*Math.pow((4 C 1),3)/(52 C 5)
//  pokerCombinations(13,4,1,1,2)*pokerCombinations(12,4,3,1,1)3
//
//  res12: Double = 0.4225690276110444
//  rank kind color kind rank other color other
//
//
//  TWO PAIR
//    This hand has the pattern AABBC where A, B, and C are from distinct kinds.
//  The number of such hands is
//    (13 C 2)(4 C 2)(4 C 2)(11 C 1)(4 C 1). After dividing by (52-choose-5), the probability is 0.047539.
//  AAB
//  (11 C 1)*(4 C 2)*(10 C 1)(4 C 1)/(50 C 3)
//
//
//  scala> var shared=(2 C 1)*(3 C 1)*(11 C 2)*Math.pow((4 C 1),2)/(50 C 3)
//
//  shared: Double = 0.2693877551020408
//  //https://www.flopturnriver.com/poker-strategy/pyroxenes-common-flop-odds-19147
}
