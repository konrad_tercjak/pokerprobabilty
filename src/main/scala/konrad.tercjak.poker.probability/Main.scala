package konrad.tercjak.poker.probability

import konrad.tercjak.poker.probability.ByRank.{Paired, UnPaired}
import konrad.tercjak.poker.probability.BySuit.{Suited, UnSuited}

object Main extends App{
  UnPaired.print
  println("")
  Paired.print
  println("")

  UnSuited.print
  println("")
  Suited.print

}
