package konrad.tercjak.util

object Birds {
  def kestrel[A](x: A)(f: A => Unit): A = { f(x); x }
  def hummingbird[R,A](after:A)(block: => R)(before :A=>Unit):R={
    val x=after
    val result=block
    before(x)
    result
  }

  /**
    * <b>Fixed Point Combinator is:</b>
    * Y = λf.(λx.f (x x)) (λx.f (x x))
    *
    * <b>Proof of correctness:</b>
    * Y g  = (λf . (λx . f (x x)) (λx . f (x x))) g    (by definition of Y)
    * = (λx . g (x x)) (λx . g (x x))                  (β-reduction of λf: applied main function to g)
    * = (λy . g (y y)) (λx . g (x x))                  (α-conversion: renamed bound variable)
    * = g ((λx . g (x x)) (λx . g (x x)))              (β-reduction of λy: applied left function to right function)
    * = g (Y g)                                        (by second equality) [1]
    */
//  def Y[T](fun: (T => T) => (T => T)): (T => T) = fun(Y(fun))(_:T) // By definition
//
//  private def fact = Y {
//    f: (Int => Int) =>
//      n: Int =>
//        if(n <= 0) 1
//        else n * f(n - 1)
//  }

//  func(Y(func))(_:T)
//  is syntax sugar for this
//  x => func(Y(func))(x)

//  What did we gain? Well, it’s the same trick as in the answer to a previous question;
//  this way we achieve that func(Y(func)) will be evaluated only
//  if needed since it’s wrapped in a function.
//  This way we will avoid an infinite loop.
//  Expanding a (single-paramter) function f into a function x => f(x)
//  is called eta-expansion (you can read more about it here).


def Y[A, B](rec: (A => B) => (A => B)): A => B = rec(Y(rec))(_: A)

}

