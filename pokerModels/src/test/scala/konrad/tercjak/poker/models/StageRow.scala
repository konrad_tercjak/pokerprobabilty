package konrad.tercjak.poker.models

import scala.beans.BeanProperty

case class StageRow(@BeanProperty var idStage:Long,@BeanProperty var  idTable: Int,@BeanProperty var dealerId: Int,@BeanProperty var time: Long)
