package konrad.tercjak.poker.models


import scala.beans.BeanProperty

case class PlayerHand(@BeanProperty var idPlayer:Short,@BeanProperty var cards: Array[Card])

case class TurnHands( @BeanProperty var idStage:Long, @BeanProperty var history:Array[PlayerHand])

case class Card(@BeanProperty var rank: Rank,@BeanProperty var suit: Suits){
//  override def toString(): String =rank.toString+suit.toString()
//  def asAscii():String=rank.toString+suit.txt

}