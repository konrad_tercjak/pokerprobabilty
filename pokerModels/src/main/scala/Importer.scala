package konrad.tercjak.poker.models

import konrad.tercjak.poker.models._


trait Importer{



  var idFile=0
  def nextFile():Importer={idFile+=1;this}

  def getStages():Array[StageRow]
  def getSeats():Array[SeatsRow]

//  def getShowdowns():Array[TurnHands]

  def getActions(): Array[StageActions]
  def getTables(): Array[TableRow]

}