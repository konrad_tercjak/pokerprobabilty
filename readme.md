**Common Flop Odds** 

Common odds when holding Unpaired hole cards:

'~' means flopping EXACTLY

|                                                        Name|Probabilty|
|------------------------------------------------------------|----------|
|                          ~ one pair by pairing a hole card |  26,9388%|
|  ~ two pair by pairing a hole card AND pairing on the board|   2,0204%|
|              ~ two pair by pairing EACH of your hole cards |   2,0204%|
|              ~ trips by flopping two cards to one hole card|   1,3469%|
|~ a full house, trips of one hole card and pairing the other|   0,0918%|
|    ~ four of a kind, three cards to one of your hole cards |   0,0102%|

Common odds when holding PAIRED hole cards:

'~' means flopping EXACTLY

|                                                        Name|Probabilty|
|------------------------------------------------------------|----------|
|                          ~ one pair by pairing a hole card |  16,1633%|
|  ~ two pair by pairing a hole card AND pairing on the board|  10,7755%|
|              ~ two pair by pairing EACH of your hole cards |   0,7347%|
|              ~ trips by flopping two cards to one hole card|   0,2449%|
|~ a full house, trips of one hole card and pairing the other|   0,2449%|

Common odds when holding UnSuited hole cards:

'~' means flopping EXACTLY

|                  Name|Probabilty|
|----------------------|----------|
|Flopping a four flush |   2,2449%|

Common odds when holding SUITED hole cards:

'~' means flopping EXACTLY

|                  Name|Probabilty|
|----------------------|----------|
|                 Flush|   0,8418%|
|Flopping a four flush |  10,9439%|



Result is same as: 
[Pyroxenes Common Flop-Odds](https://www.flopturnriver.com/poker-strategy/pyroxenes-common-flop-odds-19147/
) but counted with other method

